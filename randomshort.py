import webapp

class randomshort (webapp.webApp):
    # Declare and initialize content
    urls = {"/": ""}

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 1)[0]
        print(metodo)
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo, recurso, cuerpo = parsedRequest

        if (metodo == 'GET'):
            print("si es get")
            if recurso == '/':
                print("recurso si es barra")
                httpCode = "200 OK"
                htmlBody= "<html><body><form action = '' method = 'POST' ><p>URL a acortar: <input type = 'text' name = 'nombre' value = ''" \
                "</p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>\</body></html>"

            elif recurso in self.urls.keys() and recurso != "/":
                httpCode = "301 REDIRECT"
                htmlBody = "<html><body>URLS acortadas: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/", "",1).replace("'","",2).replace("'", "") +\
                           "<p> URLS enteras: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'", "") +\
                "</p><html><body><p><form action = '' method = 'POST' ><p>URL ¡PINCHAME!: <input type = 'text' name = 'nombre' value = ''>" \
                "</p><p><input type = 'submit' value = 'Enviar' size = '40'><p>Se te redireccionara a esta pagina en 3,2,1...</p></p></form>" \
                "<meta http-equiv = 'Refresh' content = '3; url=" + self.urls[recurso] + "'></body></html>"
            else:
                httpCode = "404 HTTP ERROR"
                htmlBody = "<html><body><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''" \
                           "</p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>\</body></html>"

            return(httpCode, htmlBody)

        if metodo == 'POST':
            cuerponuevo= cuerpo.split('=')[1]
            if(cuerponuevo != ""):
                #clave url corta
                cuerponuevo=cuerponuevo.replace("%3A", ":").replace("%2F", "/")
                print("Cuerpo: " +cuerponuevo)

                if ((cuerponuevo[0:8] == "https://") or (cuerponuevo[0:7] == "http://")):
                    claveurl=cuerpo.split('=')[1].split('%2F')[2]

                #meter en dicc el valor ya reemplazando para que salga bien
                    self.urls[claveurl]=cuerpo.split("=")[1].replace('%3A', ':').replace('%2F', '/')

                else:
                    claveurl = cuerpo.split('=')[1]
                    self.urls[claveurl]="https://"+claveurl

                httpCode = "200 OK"
                htmlBody = "<html><body>URLS acortadas: " + str(self.urls.keys()) + \
                                "<p>URLS enteras: " + str(self.urls.values())+ \
                               "</p><p><form action = '' method = 'POST' ><p>URL a acortar: <input type = 'text' name = 'nombre' value = ''>" + \
                               "</p></p><p><input type = 'submit' value = 'Enviar' size = '40'></p></form>" \
                               "<br><a href = '" + self.urls[claveurl] + "'>'" + claveurl + "'</a>" \
                               "<br><a href = '" + \
                               self.urls[claveurl] + "'>'" + self.urls[claveurl] + "'</a></body></html>"

            else:
                httpCode = "404 ERROR"
                htmlBody = "<html><body><p>Error 404</p></body></html>"

            return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)

